#include <pebble.h>
#include "lignite.h"
#include "bitmap_manager.h"
#include "gbitmap_manipulator.h"

const uint32_t bitmap_manager_bitmap_resources[AMOUNT_OF_BITMAPS] = {
	RESOURCE_ID_IMAGE_CONNECTED_ICON,
	RESOURCE_ID_IMAGE_DISCONNECTED_ICON,
	RESOURCE_ID_IMAGE_BATTERY_0_ICON,
	RESOURCE_ID_IMAGE_BATTERY_25_ICON,
	RESOURCE_ID_IMAGE_BATTERY_50_ICON,
	RESOURCE_ID_IMAGE_BATTERY_75_ICON,
	RESOURCE_ID_IMAGE_BATTERY_100_ICON,
	RESOURCE_ID_IMAGE_BATTERY_CHARGING_ICON,
	RESOURCE_ID_IMAGE_STEPS_ICON,
    RESOURCE_ID_IMAGE_HEART_ICON,
    RESOURCE_ID_IMAGE_DISTANCE_WALKED_ICON,
    RESOURCE_ID_IMAGE_TOTAL_SLEEP_ICON,
    RESOURCE_ID_IMAGE_DEEP_SLEEP_ICON,
	RESOURCE_ID_IMAGE_CALORIES_ICON,
	RESOURCE_ID_IMAGE_CALORIES_ICON,
	RESOURCE_ID_IMAGE_HEART_ICON,
	RESOURCE_ID_IMAGE_HEART_ICON
};
GBitmap *bitmap_manager_bitmaps[AMOUNT_OF_BITMAPS];
uint8_t bitmap_manager_bitmap_use_count[AMOUNT_OF_BITMAPS];
bool bitmap_manager_inverted = false;

int8_t bitmap_manager_get_index_for_resource_id(uint32_t resource_id){
	for(uint8_t i = 0; i < AMOUNT_OF_BITMAPS; i++){
		if(bitmap_manager_bitmap_resources[i] == resource_id){
			return i;
		}
	}
	return -1;
}

void bitmap_manager_assign_bitmap(InfoModule *module){
	if(module->icon_resource_id != 0){
		int8_t index_of_resource = bitmap_manager_get_index_for_resource_id(module->icon_resource_id);
		if(index_of_resource == -1){
			return;
		}
		bitmap_manager_bitmap_use_count[index_of_resource]--;
		if(bitmap_manager_bitmap_use_count[index_of_resource] == 0 && bitmap_manager_bitmaps[index_of_resource] != NULL){
			gbitmap_destroy(bitmap_manager_bitmaps[index_of_resource]);
		}
	}

	uint32_t resource_id = 0;
	GBitmap *bitmap = NULL;
	switch(module->storage_data.data_type){
		case InfoModuleDataTypeBluetoothStatus:{
			InfoModuleDataBluetooth *bluetooth_data = (InfoModuleDataBluetooth*)module->data;
			if(bluetooth_data->connected){
				resource_id = RESOURCE_ID_IMAGE_CONNECTED_ICON;
			}
			else{
				resource_id = RESOURCE_ID_IMAGE_DISCONNECTED_ICON;
			}
			break;
		}
		case InfoModuleDataTypeBatteryLevel:{
			InfoModuleDataBattery *battery_data = (InfoModuleDataBattery*)module->data;
			if(battery_data->charge_state.charge_percent < 10){
				resource_id = RESOURCE_ID_IMAGE_BATTERY_0_ICON;
			}
			else if(battery_data->charge_state.charge_percent >= 10 && battery_data->charge_state.charge_percent < 40){
				resource_id = RESOURCE_ID_IMAGE_BATTERY_25_ICON;
			}
			else if(battery_data->charge_state.charge_percent >= 40 && battery_data->charge_state.charge_percent < 60){
				resource_id = RESOURCE_ID_IMAGE_BATTERY_50_ICON;
			}
			else if(battery_data->charge_state.charge_percent >= 60 && battery_data->charge_state.charge_percent < 80){
				resource_id = RESOURCE_ID_IMAGE_BATTERY_75_ICON;
			}
			else if(battery_data->charge_state.charge_percent >= 80 && battery_data->charge_state.charge_percent <= 100){
				resource_id = RESOURCE_ID_IMAGE_BATTERY_100_ICON;
			}

			if(battery_data->charge_state.is_charging){
				resource_id = RESOURCE_ID_IMAGE_BATTERY_CHARGING_ICON;
			}
			break;
		}
		#ifdef PBL_HEALTH
		case InfoModuleDataTypeHealth:{
			InfoModuleDataHealth *health_data = (InfoModuleDataHealth*)module->data;
			int8_t index = bitmap_manager_get_index_for_resource_id(RESOURCE_ID_IMAGE_STEPS_ICON)+health_data->metric;
			resource_id = bitmap_manager_bitmap_resources[index];
			break;
		}
		#endif
		default:
			//NSError("Unrecognized module");
			break;
	}

	int8_t index_of_resource = bitmap_manager_get_index_for_resource_id(resource_id);
	if(index_of_resource == -1){
		//NSError("Resource %lu not found! (index %d)", resource_id, module->storage_data.index);
		return;
	}
	if(bitmap_manager_bitmap_use_count[index_of_resource] == 0){
		bitmap = gbitmap_create_with_resource(resource_id);
		bitmap_manager_bitmaps[index_of_resource] = bitmap;
	}
	else{
		bitmap = bitmap_manager_bitmaps[index_of_resource];
	}
	bitmap_manager_bitmap_use_count[index_of_resource]++;

	#ifdef PBL_COLOR
	if(gcolor_equal(gcolor_legible_over(data_framework_get_settings().module_colour), GColorWhite)){
		replace_gbitmap_color(GColorBlack, GColorWhite, bitmap, NULL);
	}
	#endif

	module->icon_resource_id = resource_id;
	module->icon_size = gbitmap_get_bounds(bitmap).size;
	module->icon = bitmap;
	//NSLog("Set icon %p with id %lu onto module with index %d", bitmap, resource_id, module->storage_data.index);
}

#ifdef PBL_COLOR
void bitmap_manager_reload_bitmaps(Settings new_settings){
	GColor new_legible_colour = gcolor_legible_over(new_settings.module_colour);
	bool new_inverted = gcolor_equal(new_legible_colour, GColorWhite);
	uint8_t count = 0;
	if(new_inverted != bitmap_manager_inverted){
		for(uint8_t i = 0; i < AMOUNT_OF_BITMAPS; i++){
			if(bitmap_manager_bitmaps[i]){
				count++;
				replace_gbitmap_color(bitmap_manager_inverted ? GColorWhite : GColorBlack, new_legible_colour, bitmap_manager_bitmaps[i], NULL);
			}
		}
	}
	if(count > 0){
		bitmap_manager_inverted = new_inverted;
	}
}
#endif