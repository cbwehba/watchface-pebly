#include <pebble.h>
#include "lignite.h"
#include "main_window.h"
#include "info_layer.h"
#include "netdownload.h"

#define NIGHT_HOUR_FIRST 23
#define NIGHT_HOUR_LAST 6

Window *main_window;
Layer *window_layer, *main_window_graphics_layer = NULL;
Settings settings;

AppTimer *new_photo_timer;

BitmapLayer *photo_layers[2];
GBitmap *current_photos[2], *island_parts[2];
bool loaded_photo = false;
bool night_time = false;
bool boot = true;

static char time_buffer[] = "00:00";

void main_tick_handler(struct tm *t, TimeUnits units){
	strftime(time_buffer, sizeof(time_buffer), clock_is_24h_style() ? "%H:%M" : "%I:%M", t);

	//NSLog("Keep alive");

	night_time = (t->tm_hour >= NIGHT_HOUR_FIRST && t->tm_hour <= NIGHT_HOUR_LAST);

	if(main_window_graphics_layer){
		layer_mark_dirty(main_window_graphics_layer);
	}
}

void reset_other_text(){
	info_layer_set_other_text(NULL);
}

void bluetooth_handler(bool connected){
	info_layer_bluetooth_handler(connected);

	if(connected && settings.btrealert){
		vibes_double_pulse();
		info_layer_set_other_text("Bluetooth reconnected");
	}
	else if(!connected && settings.btdisalert){
		vibes_long_pulse();
		info_layer_set_other_text("Bluetooth disconnected");
	}
	app_timer_register(7500, reset_other_text, NULL);
}

void main_window_settings_callback(Settings new_settings){
	settings = new_settings;

	struct tm *t;
  	time_t temp;
  	temp = time(NULL);
  	t = localtime(&temp);
  	main_tick_handler(t, MINUTE_UNIT);
}

void apply_island(){
	#ifndef PBL_ROUND
	if(island_parts[0] != NULL){
		return;
	}
	if(!settings.show_island_image){
		return;
	}
	uint32_t parts[] = {
		RESOURCE_ID_ISLAND_PART_1, RESOURCE_ID_ISLAND_PART_2
	};
	for(uint8_t i = 0; i < 2; i++){
		if(current_photos[i]){
			bitmap_layer_set_bitmap(photo_layers[i], NULL);
			gbitmap_destroy(current_photos[i]);
			current_photos[i] = NULL;
		}
		island_parts[i] = gbitmap_create_with_resource(parts[i]);
		bitmap_layer_set_bitmap(photo_layers[i], island_parts[i]);
	}
	#endif
}

void download_complete_handler(NetDownload *download, bool right_half) {
	//Is saying that we need to put the stock image in place
	if(download == NULL){
		apply_island();
		return;
	}
	//printf("Loaded image with %lu bytes, heap free is %u bytes", download->length, heap_bytes_free());

	if(right_half){
		bitmap_layer_set_bitmap(photo_layers[0], current_photos[0]);
		bitmap_layer_set_bitmap(photo_layers[1], NULL);
		#ifndef PBL_ROUND
		if(island_parts[0]){
			gbitmap_destroy(island_parts[0]);
			gbitmap_destroy(island_parts[1]);
			island_parts[0] = NULL;
			island_parts[1] = NULL;
		}
		#endif
	}

	// Save pointer to currently shown bitmap (to free it)
	if (current_photos[right_half]) {
		gbitmap_destroy(current_photos[right_half]);
	}
	GBitmap *photo = gbitmap_create_from_png_data(download->data, download->length);
	if(right_half){
		bitmap_layer_set_bitmap(photo_layers[right_half], photo);
	}

	current_photos[right_half] = photo;

	free(download->data);

	download->data = NULL;
	netdownload_destroy(download);

	loaded_photo = right_half;
	layer_mark_dirty(main_window_graphics_layer);
}

void main_window_graphics_proc(Layer *layer, GContext *ctx){
	GRect layer_frame = layer_get_unobstructed_bounds(layer);

	GSize info_layer_size = info_layer_get_size(true);
	GSize info_layer_unmodified_size = info_layer_get_size(false);
	bool info_layer_is_hidden = (info_layer_unmodified_size.h != info_layer_size.h);

	uint8_t idiot_adjust = 10;
	uint8_t offset_from_bottom = 20;

	GFont time_font = fonts_get_system_font(FONT_KEY_GOTHIC_28_BOLD);
	bool apply_large_time_font = settings.large_time_font && (info_layer_get_amount_of_modules() < 5);

	if(apply_large_time_font){
		time_font = fonts_get_system_font(FONT_KEY_BITHAM_34_MEDIUM_NUMBERS);
	}
	GSize time_size = graphics_text_layout_get_content_size(time_buffer, time_font, GRect(0, 0, layer_frame.size.w, layer_frame.size.h), GTextOverflowModeWordWrap, GTextAlignmentCenter);

	bool hide_all = (settings.shake_mode == ShakeModeHideAll && info_layer_is_hidden);

	// NSLog("%d %d %d %d", hide_all, settings.shake_mode, info_layer_size.h, info_layer_unmodified_size.h);

	time_size.h -= idiot_adjust;
	GRect time_frame = GRect((layer_frame.size.w/2) - (time_size.w/2), layer_frame.size.h-offset_from_bottom-time_size.h-info_layer_size.h-4 + (hide_all ? (info_layer_unmodified_size.h-info_layer_size.h)*2 : 0), time_size.w, time_size.h);
	while(time_frame.origin.y < PBL_IF_ROUND_ELSE(16, 0) && !hide_all){
		time_frame.origin.y += 2;
	}

	#ifdef PBL_ROUND
	if(apply_large_time_font && info_layer_size.h < 18){
		time_frame.origin.y -= 18-info_layer_size.h;
	}
	else if(!apply_large_time_font && info_layer_size.h < 8){
		time_frame.origin.y -= 8-info_layer_size.h;
	}
	#endif

	uint8_t padding = 6;
	uint8_t double_padding = (padding*2);
	GRect time_box_frame = GRect(time_frame.origin.x-padding, time_frame.origin.y+idiot_adjust-padding, time_frame.size.w+double_padding, time_frame.size.h+double_padding);

	if(apply_large_time_font){
		time_frame.origin.x -= 1;
	}

	int time_padding = 4;
	if(apply_large_time_font){
		time_padding = 8;
	}
	time_box_frame.origin.x -= time_padding;
	time_box_frame.size.w += time_padding*2;

	graphics_context_set_fill_color(ctx, settings.background_colour);
	graphics_fill_rect(ctx, time_box_frame, PBL_IF_ROUND_ELSE(time_frame.size.w/2, 3), GCornersAll);
	graphics_context_set_text_color(ctx, gcolor_legible_over(settings.background_colour));
	graphics_draw_text(ctx, time_buffer, time_font, time_frame, GTextOverflowModeWordWrap, GTextAlignmentCenter, NULL);

	//GRect rounded_corners_rect = GRect(0, 0, info_layer_size.w, layer_frame.size.h-info_layer_size.h);

	#ifndef PBL_ROUND
	graphics_context_set_stroke_color(ctx, GColorBlack);

	//Top right
	int xcoord = layer_frame.size.w-1;
	//Bottom left
	int ycoord = layer_frame.size.h-info_layer_size.h-1;
	graphics_draw_pixel(ctx, GPoint(0, ycoord));
	graphics_draw_pixel(ctx, GPoint(1, ycoord));
	graphics_draw_pixel(ctx, GPoint(0, ycoord-1));

	//Bottom right
	graphics_draw_pixel(ctx, GPoint(xcoord, ycoord));
	graphics_draw_pixel(ctx, GPoint(xcoord-1, ycoord));
	graphics_draw_pixel(ctx, GPoint(xcoord, ycoord-1));
	#endif

	//graphics_draw_round_rect(ctx, rounded_corners_rect, 3);
}

void request_new_photo(){
	if(settings.photo_timeout == 666 && !boot){
		//No fucking need
		return;
	}
	if(settings.photo_timeout < 900000 && settings.photo_timeout != 666){
		settings.photo_timeout = 900000;
	}
	new_photo_timer = app_timer_register(settings.photo_timeout, request_new_photo, NULL);

	if(boot){
		boot = false;
		return;
	}

	if(night_time && settings.night_mode && !boot){
		return;
	}

	DictionaryIterator *iter = NULL;
	app_message_outbox_begin(&iter);

	if(!iter){
		return;
	}

	dict_write_uint8(iter, MESSAGE_KEY_pleaseGiveMeANewPhoto, 1);
	dict_write_uint16(iter, MESSAGE_KEY_photoTransferRequestedChunkSize, APP_MESSAGE_SIZE);
	dict_write_end(iter);
	app_message_outbox_send();
}

void main_window_info_layer_update_callback(){
	layer_mark_dirty(main_window_graphics_layer);
}

void main_window_load(Window *window){
	window_layer = layer_create(WINDOW_FRAME);
	layer_add_child(window_get_root_layer(window), window_layer);

	data_framework_register_settings_callback(main_window_settings_callback, SETTINGS_CALLBACK_MAIN_WINDOW);

	settings = data_framework_get_settings();
	main_window_settings_callback(settings);

	GRect bounds = layer_get_bounds(window_layer);

	for(int i = 0; i < 2; i++){
		photo_layers[i] = bitmap_layer_create(GRect((bounds.size.w/2)*i, 0, bounds.size.w/2, bounds.size.h));
		layer_add_child(window_layer, bitmap_layer_get_layer(photo_layers[i]));
		current_photos[i] = NULL;
	}

	main_window_graphics_layer = layer_create(bounds);
	layer_set_update_proc(main_window_graphics_layer, main_window_graphics_proc);
	layer_add_child(window_layer, main_window_graphics_layer);

	info_layer_create(WINDOW_FRAME, main_window_info_layer_update_callback);
	layer_add_child(window_layer, info_layer_get_layer());

	netdownload_initialize(download_complete_handler);

	apply_island();
	request_new_photo();
}

void unobstructed_area_changed(){
	layer_mark_dirty(main_window_graphics_layer);
}

void main_window_create(){
	main_window = window_create();
	window_set_background_color(main_window, GColorBlack);
	window_set_window_handlers(main_window, (WindowHandlers){
		.load = main_window_load
	});
	tick_timer_service_subscribe(MINUTE_UNIT, main_tick_handler);
	connection_service_subscribe((ConnectionHandlers){
		.pebble_app_connection_handler = bluetooth_handler
	});
	unobstructed_area_service_subscribe((UnobstructedAreaHandlers){
		.did_change = unobstructed_area_changed
	}, NULL);
}

Window *main_window_get_window(){
	return main_window;
}
