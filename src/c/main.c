#include <pebble.h>
#include "lignite.h"
#include "main_window.h"
#include "info_layer.h"

int main() {
	info_layer_load_modules();
	data_framework_setup();
    main_window_create();
    window_stack_push(main_window_get_window(), true);
    app_event_loop();
	info_layer_save_modules();
    data_framework_finish();
}
