#pragma once

#ifdef PBL_ROUND
#define WINDOW_FRAME GRect(0, 0, 180, 180)
#else
#define WINDOW_FRAME GRect(0, 0, 144, 168)
#endif

void main_window_create();
Window *main_window_get_window();
void main_window_update_settings(Settings new_settings);
